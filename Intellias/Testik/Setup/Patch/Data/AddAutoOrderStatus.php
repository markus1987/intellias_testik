<?php

namespace Intellias\Testik\Setup\Patch\Data;

use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Sales\Model\Order\StatusFactory;
use Magento\Sales\Model\ResourceModel\Order\Status;
use Magento\Sales\Model\ResourceModel\Order\Status\CollectionFactory as OrderStatusCollection;
use Magento\Sales\Model\ResourceModel\Order\StatusFactory as StatusResourceFactory;

class AddAutoOrderStatus implements DataPatchInterface
{
    const STATUS_CODE = 'Auto status';

    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * @var StatusFactory
     */
    protected $statusFactory;


    /**
     * @var Status
     */
    protected $resourceModel;

    protected $orderStatusCollectionFactory;

    /**
     * @var StatusResourceFactory
     */
    protected $statusResourceFactory;

    /**
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param StatusFactory $statusFactory
     * @param StatusResourceFactory $statusResourceFactory
     */
    public function __construct(
        Status                   $resourceModel,
        OrderStatusCollection    $collectionFactory,
        ModuleDataSetupInterface $moduleDataSetup,
        StatusFactory            $statusFactory,
        StatusResourceFactory    $statusResourceFactory
    )
    {
        $this->resourceModel = $resourceModel;
        $this->orderStatusCollectionFactory = $collectionFactory;
        $this->moduleDataSetup = $moduleDataSetup;
        $this->statusFactory = $statusFactory;
        $this->statusResourceFactory = $statusResourceFactory;
    }

    /**
     * @inheritdoc
     */
    public function apply()
    {
        $status = $this->statusFactory->create();
        $options = $this->getOrderStatusOptions();
        $status->setData([
            'status' => self::STATUS_CODE,
            'label' => 'Auto status',
        ]);

        /**
         * Save the new status
         */
        $statusResource = $this->statusResourceFactory->create();
        $statusResource->save($status);

        /**
         * Assign status to state
         */
        $states = $this->getOrderStatusOptions();
        array_shift($states);
        foreach ($states as $state => $stateValue) {
            $status->assignState($stateValue['state'], false, true);
        }


        return $this;
    }

    /**
     * @inheritdoc
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function getAliases()
    {
        return [];
    }

    public function getOrderStatusOptions(): array
    {
        $connection = $this->resourceModel->getConnection();
        $select = $connection->select()->from('sales_order_status_state', 'state');
        return $connection->fetchAll($select);
    }
}
