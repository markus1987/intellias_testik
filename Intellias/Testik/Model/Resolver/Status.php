<?php

namespace Intellias\Testik\Model\Resolver;

use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Exception\GraphQlInputException;
use Magento\Framework\GraphQl\Exception\GraphQlNoSuchEntityException;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Magento\Sales\Model\ResourceModel\Order\Status as Resourse;

class Status implements ResolverInterface

{

    protected $resourceModel;

    public function __construct(Resourse $resourceModel)
    {
        $this->resourceModel = $resourceModel;
    }


    public function resolve(Field $field, $context, ResolveInfo $info, array $value = null, array $args = null)

    {
        if (empty($args['status'])) {
            throw new GraphQlInputException(__('Status  value should be specified'));
        }

        try {
            $connection = $this->resourceModel->getConnection();
            $select = $connection->select()->from('sales_order_status_state', '*')->where('status = ?', $args['status']);
            $result = $connection->fetchAll($select);
        } catch (NoSuchEntityException $exception) {
            throw new GraphQlNoSuchEntityException(__($exception->getMessage()), $exception);
        }

        return $result[0];

    }

}
